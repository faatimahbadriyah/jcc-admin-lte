<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kritik extends Model
{
    public function user()
    {
        return $this->belongsToMany('App\User');
    }

    public function film()
    {
        return $this->belongsToMany('App\Film');
    }
}