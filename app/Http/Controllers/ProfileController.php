<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $loggedUser = Auth::user();
        $user = User::where('id', $loggedUser['id'])->first();
        $activeMenu = $this->activeMenu('profile', '');
        $data = [
            'title' => 'PROFILE',
            'parent' => 'Home',
            'child' => 'profile',
            'user' => $user,
            'menu' => $activeMenu['menu'],
            'submenu' => $activeMenu['submenu'],
        ];
        return view('profile.index', $data);
    }

    public function edit()
    {
        $loggedUser = Auth::user();
        $user = User::where('id', $loggedUser['id'])->first();
        $activeMenu = $this->activeMenu('profile', '');
        $data = [
            'title' => 'CAST EDIT',
            'parent' => 'Home',
            'child' => 'cast',
            'user' => $user,
            'menu' => $activeMenu['menu'],
            'submenu' => $activeMenu['submenu'],
        ];
        return view('profile.edit', $data);
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        $loggedUser = Auth::user();
        $user = User::where('id', $loggedUser['id'])->first();
        $user->name = $request["nama"];
        $user->email = $request["email"];

        if (!$user->update()) {
            DB::rollBack();
            return false;
        } else {
            $profile = Profile::updateOrCreate(
                ['user_id' => $user->id],
                [
                    'alamat' => $request["alamat"],
                    'umur' => $request["umur"],
                    'bio' => $request["bio"],
                ]
            );
            if (!$profile) {
                DB::rollBack();
                return false;
            }
        }
        DB::commit();

        return redirect('/profile');
    }
}