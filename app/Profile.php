<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public $timestamps = false;
    protected $table = "profile";
    protected $fillable = ["alamat", "umur", "bio"];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}