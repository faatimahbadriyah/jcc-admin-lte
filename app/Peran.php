<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    public function cast()
    {
        return $this->belongsToMany('App\Cast');
    }

    public function film()
    {
        return $this->belongsToMany('App\Film');
    }
}