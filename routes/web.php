<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();
Route::resource('cast', 'CastController');
Route::middleware(['auth'])->group(function () {
    Route::get('/', 'AdminController@index');

    Route::prefix('profile')->group(function () {
        Route::get('/', 'ProfileController@index');
        Route::get('/edit', 'ProfileController@edit');
        Route::put('/update', 'ProfileController@update');
    });

    Route::get('/table', 'AdminController@table');
    Route::get('/data-table', 'AdminController@dataTable');
    Route::get('/home', 'HomeController@index')->name('home');
});